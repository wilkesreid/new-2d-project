﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour {

	public float gravity;
	public float movementSpeed = 7;
	public LayerMask groundLayer;
	protected Rigidbody2D rb;
	protected Vector2 velocity;
	protected RaycastHit2D groundTest;
	protected Vector2 move;
	protected bool grounded = false;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		move.x = Input.GetAxis("Horizontal") * movementSpeed;
	}

	void FixedUpdate() {
		// Calculate gravity
		if (!IsGrounded()) {
			// Fall
			velocity += gravity * Physics2D.gravity;
		} else {
			velocity.y = 0;
			// Can jump
			if (Input.GetButtonDown("Jump")) {
				velocity.y = 4;
			}
		}

		velocity.x = move.x;

		Vector2 deltaPosition = velocity * Time.deltaTime;

		// Update position
		rb.position = rb.position + deltaPosition;
	}

	bool IsGrounded() {
		RaycastHit2D hit = Physics2D.Raycast(rb.position, Vector2.down, 1.0f, groundLayer);
		if (hit.collider != null) {
			return true;
		}
		return false;
	}

	void OnGUI() {
		GUI.Label(new Rect(10, 10, 100, 20), velocity.ToString());
	}
}
